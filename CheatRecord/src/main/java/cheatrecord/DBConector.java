/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cheatrecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class DBConector {

    Connection conectar = null;

    public Connection conexion() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");//.newInstance();
            conectar = DriverManager.getConnection("jdbc:mysql://localhost/cheatrecord", "root", "12345");
            
            //conectar=DriverManager.getConnection("jdbc:mysql://192.168.1.5:3306/NombreBaseDeDatos","Usuario","password");
            //JOptionPane.showMessageDialog(null, "Base de datos cargada...");
            //Puede usar un JOption pane para dar una alerta de carga de la base de datos pero es solo para dar presentacion
            //no estan necesario realmente :)
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se pudo cargar la base de datos...!!");
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Error al buscar la clase!!" + ex.getMessage());
        }
        return conectar;
    }
}
