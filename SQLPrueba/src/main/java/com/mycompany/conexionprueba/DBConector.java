/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.conexionprueba;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class DBConector {

    Connection conectar = null;

    public Connection conexion() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");//.newInstance();
            conectar = DriverManager.getConnection("jdbc:mysql://localhost/alumnos1", "root", "12345");
            //conectar=DriverManager.getConnection("jdbc:mysql://192.168.1.5:3306/personas","luis","");

            JOptionPane.showMessageDialog(null, "Base de datos actualizada");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion de la base de datos");
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "No se encontro la clase " + ex.getMessage());
        }
        return conectar;
    }
}
