/**
 * INSTITUTO TECNOLOGICO SUPERIOR DE CHAMPOTÓN
 * ING. SISTEMAS COMPUTACIONALES
 * MATERIA: PROGRAMACION ORIENTADA A OBJETOS
 * EJEMPLO EN JAVA
 * SUBIDO POR JOSMUSER
 */
package Calculadora;
import java.util.Scanner;
public class Sumar {
  
    boolean unlock;
    //Constructor
    int factorfinalsuma;

    public Sumar() {
        this.unlock = false;
        factorfinalsuma = factorfinalsuma;
    }
    //Metodo
    @SuppressWarnings("ConvertToTryWithResources")
    public void Operacionsumar() {
        //PARA LA LONGITUD
        Scanner elementos = new Scanner(System.in);
        //PARA INTRODUCIR MIS DATOS
        Scanner minumero = new Scanner(System.in);
        //ARREGLOS
        int[] numeroelementos;
        System.out.println("Cuantos numeros desea sumar");
        int n = elementos.nextInt();//INTRODUCENN CUANTOS NUMEROS QUEREMOS SUMAR
        numeroelementos = new int[n]; // N ES MI LONGITUD DE NUMEROS A SUMAR
        int total = 0;
        do {
            System.out.println("A continuacion debera digitar los numeros a sumar");
            for (int i = 0; i < n; i++) {
                System.out.println((i + 1) + "Digite un numero:");
                numeroelementos[i] = minumero.nextInt();//SE ALMACENAN EN EL ARRAY
                total = total + numeroelementos[i]; //SE SUMA LOS ELEMENTOS DEL ARRAY TOMANDO QUE TOTAL ES 0
                int f = 0;
                f++;//SE ITERA F PARA IGUALAR SI TIENE LA LONGITUD QUE EL ARRAY
                if (f == numeroelementos[i]) {
                    unlock = Boolean.TRUE; //EN CASO DE SER VERDAD SE CAMBIA EL VALOR DEL ATRIBUTO UNLOCKA TRUE
                }
                factorfinalsuma= total;// SE PASO EL RESULTADO AL ATRIBUTO FACTORFINALSUMA
            }        
        } while (unlock == true);
        System.runFinalization();//CON ESTO FINALIZAMOS METODOS PAR ANO DEJARLOS ABIERTOS
    }
    public void resultadoSumar(){
     System.out.println("Resultado: " + factorfinalsuma);
     Interfaz.main(null);//CON ESTE REGRESAMOS A LA INTERFAZ O CLASE PRINCIPAL
    }
}