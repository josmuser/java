/**
 * INSTITUTO TECNOLOGICO SUPERIOR DE CHAMPOTÓN
 * ING. SISTEMAS COMPUTACIONALES
 * MATERIA: PROGRAMACION ORIENTADA A OBJETOS
 * EJEMPLO EN JAVA
 * SUBIDO POR JOSMUSER
 */
package Calculadora;

import java.util.Scanner;

public class Multiplicar {
//ATRIBUTOS

    private int factorfinalmultiplicar;
    boolean unlock;

    //CONSTRUCTOR
    public Multiplicar() {
        this.unlock = false;
        factorfinalmultiplicar = factorfinalmultiplicar;
    }

    //METODOS
    @SuppressWarnings("ConvertToTryWithResources")
    public void Operacion_multiplicar() {
        //Introducir datos
        //Para la longitud
        Scanner elementos = new Scanner(System.in);
        //Para mis numeros
        Scanner minumero = new Scanner(System.in);
        //ARREGLOS
        int[] numeroelementos;
        System.out.println("Cuantos numeros desea multiplicar");
        int n = elementos.nextInt();
        numeroelementos = new int[n];
        int total = 1;
        do {
            System.out.println("A continuacion debera digitar los numeros a multiplicar");
            for (int i = 0; i < n; i++) {
                System.out.println((i + 1) + "Digite un numero:");
                numeroelementos[i] = minumero.nextInt();
                total = (total * numeroelementos[i]);
                int f = 0;
                f++;
                if (f == numeroelementos[i]) {
                    unlock = Boolean.TRUE;
                }
                factorfinalmultiplicar = total;
            }
        } while (unlock == true);
    }

    public void Resultado_Division() {
        System.out.println("Resultado: " + factorfinalmultiplicar);
        Interfaz.main(null);
    }
}
