/**
 * INSTITUTO TECNOLOGICO SUPERIOR DE CHAMPOTÓN
 * ING. SISTEMAS COMPUTACIONALES
 * MATERIA: PROGRAMACION ORIENTADA A OBJETOS
 * EJEMPLO EN JAVA
 * SUBIDO POR JOSMUSER
 */
package Calculadora;
import java.util.Scanner;
public class Dividir {
    //ATRIBUTOS
    int elemento1=0;
    int elemento2=0;
    int resultado_operacion;
    int factorfinaldivision;
    //CONSTRUCTOR
    public Dividir() {
        resultado_operacion=resultado_operacion;
        factorfinaldivision = factorfinaldivision;
    }
    //METODOS 
    @SuppressWarnings("ConvertToTryWithResources")
    public void Operaciondivision() {
        //SE CREA UN SCANNER PARA INTRODUCIR DATOS
        Scanner numdivision = new Scanner(System.in);
        //SE ABRE UN ARRAY DE TIPO DE INTERO CON UN UNA LONGITUD DE 2 ESPACIOS
        int[] almacen_division = new int[2];
        //SE PIDE QUE INGRESE SUS DATOS
        System.out.println("Ingrese dos numeros para dividir");
        for (int i = 0; i < almacen_division.length; i++) {
            System.out.println("Digite un numero");
            almacen_division[i] = numdivision.nextInt();
        }    
        for(int i=0;i < almacen_division.length;i++){
        elemento1=almacen_division[0];//ASIGNO LA POSICION 0 A MI ATRIBUTO ELEMENTO1
        elemento2=almacen_division[1];//ASIGNO LA POSICION 0 A MI ATRIBUTO ELEMENTO2
        resultado_operacion=elemento1/elemento2;//DIVIDO LOS PARAMETROS PASADOS A MIS ATRIBUTOS Y DIVIDO
        }
        factorfinaldivision=resultado_operacion;//ASIGNO EL RESULTADO QUE LE PASO A MI ATRIBUTO RESULTADO OPERACION
        //SE LO PASO A MI FACTOR FINAl
    }
    public void resultadodivision() {
        //CREO UN METODO DE IMPRESION DE RESULTADO
        System.out.println("Resultado: " + factorfinaldivision);
        Interfaz.main(null);//CON ESTE REGRESAMOS A LA INTERFAZ O CLASE PRINCIPAL
    }
}
