/**
 * INSTITUTO TECNOLOGICO SUPERIOR DE CHAMPOTÓN
 * ING. SISTEMAS COMPUTACIONALES
 * MATERIA: PROGRAMACION ORIENTADA A OBJETOS
 * EJEMPLO EN JAVA
 * SUBIDO POR JOSMUSER
 */
package Calculadora;

import java.util.Scanner;

public class Interfaz {

    public static void main(String[] args) {
        Multiplicar usuarioMultiplicar = new Multiplicar();
        Sumar usuarioSumar = new Sumar();
        Dividir usuarioDividir = new Dividir();
        Restar usuarioRestar = new Restar();
        Scanner mi_opcion = new Scanner(System.in);
        String operacion;
        System.out.println("Calculadora Basica v0.1");
        System.out.println("¿Que operacion deseas hacer?");
        System.out.println("***Cuidado escribir tal como se muestra la opciones***");
        String[] opciones = {"Sumar", "Restar", "Dividir", "Multiplicar", "Salir"};
        for (int i = 0; i < opciones.length; i++) {
            System.out.println("Opcion [" + i + "] " + opciones[i]);
        }
        System.out.println("Elijo: ");
        operacion = mi_opcion.nextLine();
        switch (operacion) {
            case "Sumar":
                usuarioSumar.Operacionsumar();
                usuarioSumar.resultadoSumar();
                break;
            case "Dividir":
                usuarioDividir.Operaciondivision();
                usuarioDividir.resultadodivision();
                break;
            case "Multiplicar":
                usuarioMultiplicar.Operacion_multiplicar();
                usuarioMultiplicar.Resultado_Division();
                break;
            case "Restar":
                usuarioRestar.Operacionrestar();
                usuarioRestar.resultadoRestar();
                break;
            case "Salir":
                mi_opcion.close();
                System.exit(0);
                System.runFinalization();//CON ESTO FINALIZAMOS METODOS PAR ANO DEJARLOS ABIERTOS
                break;
            default:
                Interfaz.main(null);//CON ESTE REGRESAMOS A LA INTERFAZ O CLASE PRINCIPAL
                break;
        }
    }
}