/**
 * INSTITUTO TECNOLOGICO SUPERIOR DE CHAMPOTÓN
 * ING. SISTEMAS COMPUTACIONALES
 * MATERIA: PROGRAMACION ORIENTADA A OBJETOS
 * Ejemplo de factorial recursivo usando bucles recursivos
 * subido por josmuser @lias hollow code
 */
import java.util.ArrayList;
import java.util.Scanner;

public class Factorial {

    /**
     * EXPLICACION SE PASA LA LISTA DE NUMEROS INGRESADOS POR EL USUARIO DE TAL
     * MANERA QUE SE TENDRAN QUE ITERAR PARA SABER SU FACTORIAL Y DEVOLVERLO
     * PARA SER VISUALIZADOS EN PANTALLA., USANDO TAMBIEN LA RECURSIVIDAD.
     *
     * @param numero
     * @return
     */
    //SE SACA EL FACTORIAL USANDO UN BUCLE RECURSIVO ;)
    public static int sacando_fact(int numero) {
        int productoria = 1;
        for (int i = 1; i <= numero; i++) {//USO UN BUCLE RECURSIVO, MISMA FORMA QUE SE PUEDE HACER 
            productoria = productoria * i;
        }
        return productoria;
    }

    //SE DECLARAN LAS VARIABLES
    int tamanio;
    int productoriafinal;
    //CONSTRUCTOR
    public Factorial() {
        this.productoriafinal = productoriafinal;
    }
    //METODOS
    public void Operacion() {
        //DECLARACION DEL ARRAYLIST
        ArrayList<Integer> lista = new ArrayList<>();
        
        //UN NUEVO SCANNER PARA DATOS     
        Scanner dato_longitud = new Scanner(System.in);
        Scanner dato_ingresados = new Scanner(System.in);
        Scanner cambiarnumero = new Scanner(System.in);
        Scanner cambiarentero = new Scanner(System.in);
        
        System.out.println("¿ A cuantos numeros le quiere sacar su factorial ? **Escriba numeros solamentesi**");
        tamanio = dato_longitud.nextInt();
        
        int cambiovalor;//VARIABLE PARA CAMBIAR EL VALOR EN EL ARRAYLIST 
        for (int i = 1; i <= tamanio; i++) {
            System.out.println("Digite su numero [" + i + "]");
            int num = dato_ingresados.nextInt();
            lista.add(num);
        }
        
        //METODOS PARA CAMBIAR LOS VALORES SET y GET CON BUCLE FOR
        System.out.println("¿Desea cambiar todos los valores de su lista de numeros? **Escriba explicitamente si o no en minusculas**");
        String decision = cambiarnumero.nextLine();
        
        System.out.println("Sus valores actuales son los siguientes:");
        for (int i = 0; i < lista.size(); i++) {
            //USANDO LOS GET PERO EN UN BUCLE ACORTANDO EL CODIGO HACIENDO EL MINIMO           
            System.out.println("En la posicion (" + i + ") tiene el valor actual de (" + lista.get(i) + ")");
            System.out.println(" ");
        }
        
        //SE DECIDE SI CAMBIAR NUMEROS O NO
        switch (decision) {
            case "si":
                System.out.println("Advertencia! Los numeros se reemplazaran de acuerdoa su posicion.");
                for (int i = 0; i < lista.size(); i++) {
                    System.out.println("Cual es el valor que reemplazara al numero en la posicion " + i);
                    cambiovalor = cambiarentero.nextInt();
                    //USANDO SETTERS PARA REEMPLAZAR LOS NUMEROS PERO CON BUCLE
                    lista.set(i, cambiovalor);
                }
                for (int i = 0; i < lista.size(); i++) {
                    System.out.println("Se sacara los factoriales de los siguientes numeros:" + lista.get(i));
                }
                for (Integer e : lista) {
                    System.out.println("Factoriales resultantes:" + sacando_fact(e));
                }
                break;
            case "no":
                for (int i = 0; i < lista.size(); i++) {
                    System.out.println("Se sacara los factoriales de los siguientes numeros:" + lista.get(i));
                }
                for (Integer e : lista) {
                    System.out.println("Factoriales resultantes:" + sacando_fact(e));
                }
                break;
            default:
                System.out.println("Hubo un error...reiniciando programa");
                lista.clear();
                Usuario.main(null);
        }
    }

    public void salir() {
        Scanner salir = new Scanner(System.in);
        String definido_opcion;
        System.out.println("**************************");
        System.out.println("¿Quiere realmente salir? Escriba " + "si" + " o " + "no");
        definido_opcion = salir.nextLine();
        switch (definido_opcion) {
            case "si":
                System.out.println("Programa finalizado...");
                System.exit(0);
                break;
            case "no":
                Usuario.main(null);
                break;
            default:
                System.out.println("Programa cerrado inesperadamente...");
                Usuario.main(null);
        }
    }
}
