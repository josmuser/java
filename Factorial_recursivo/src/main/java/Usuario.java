/**
 * INSTITUTO TECNOLOGICO SUPERIOR DE CHAMPOTÓN
 * ING. SISTEMAS COMPUTACIONALES
 * MATERIA: PROGRAMACION ORIENTADA A OBJETOS
 * Ejemplo de factorial recursivo usando bucles recursivos
 * subido por josmuser @lias hollow code
 */

import java.util.Scanner;

public class Usuario {

    public static void main(String[] args) {
        //OBJETO FACTORIAL
        Factorial usuario = new Factorial();
        Scanner opcion = new Scanner(System.in);
        String definido_opcion;//OPCION PARA SALIR O ENTRAR AL PROGRAMA
        String[] opciones = {"INSTITUTO TECNOLOGICO SUPERIOR DE CHAMPOTÓN", "ING. SISTEMAS COMPUTACIONALES", "MATERIA: PROGRAMACION ORIENTADA A OBJETOS", "DOCENTE: KELVIN DEL JESÚS DELGADO CHAN", "ALUMNO: JOSIAS LEVID HIDALGO HERNANDEZ", "MATRICULA: 201080104", "ACTIVIDAD 1: FACTORIAL CON RECURSIVIDAD"};
        for (int i = 0; i < opciones.length; i++) {
            System.out.println(opciones[i]);
        }
        System.out.println("");
        System.out.println("***Este programa saca varios factoriales de multiples numeros***");
        System.out.println("¿Quiere sacar factoriales? Escriba" + " si " + " o " + " no ");
        definido_opcion = opcion.nextLine();
        switch (definido_opcion) {
            case "si":
                usuario.Operacion();
                break;
            case "no":
             usuario.salir();
                break;
            default:
                Usuario.main(null);
        }
    }
}