
import java.util.Scanner;
/**
 * PROGRAMA DE PILAS
 * FECHA DE CREACION: 19/05/2021
 */

import java.util.Stack;


public class Stack_class {

    int apilar;
    int buscar;

    //MI PILA DECLARADA PARA USARLA EN CUALQUIER METODO
    public Stack_class() {
        this.apilar = 0;
        this.buscar = 0;

    }

    @SuppressWarnings("empty-statement")
    public void apilar() {
        Stack<Integer> pila = new Stack<>();
        //VARIABLE PARA MIS METODOS
        Scanner limiteStack = new Scanner(System.in);
        Scanner nuevodato = new Scanner(System.in);
        int longitud;
        System.out.println("Cuantos elementos desea insertar en su pila");
        longitud = limiteStack.nextInt();
        for (int i = 0; i < longitud; i++) {
            System.out.println("Ingrese nuevo dato! >");
            apilar = nuevodato.nextInt();
            pila.push(apilar);
        }
        System.out.println("Datos almacenados correctamente!");
        Scanner mio = new Scanner(System.in);
        System.out.println("Opcion:");
        System.out.println("1-Retirar\n2-cima\n3-esta vacia\n4-buscar\n5-eliminar\n6-Listar\n");
        int op = mio.nextInt();
        switch (op) {
            case 1:
                System.out.println("Retirado " + pila.pop());
                break;
            case 2:
                System.out.println("Cima" + pila.peek());
                break;
            case 3:
                if (pila.empty() == true) {
                    System.out.println("Pila si esta vacia!!");
                } else {
                    if (pila.empty() == false) {
                        System.out.println("Pila no esta vacia!!");
                    }
                }
                break;
            case 4:
                Scanner number = new Scanner(System.in);
                System.out.println("Que numero desea buscar?");
                buscar = number.nextInt();
                boolean band = false;
                while (!pila.empty() && !band) {
                    if (pila.pop() == buscar) {
                        band = true;
                    }
                }
                if (band == true) {
                    System.out.println("Dato SI se encuentra en pila!");
                } else {
                    if (band == false) {
                        System.out.println("Dato NO se encuentra en pila!");
                    }
                }
                break;
            case 5:
                System.out.println("Vaciando pila");
                pila.removeAllElements();
                System.out.println("Todos los elementos removidos exitosamente!!");
                break;
            case 6:
                System.out.println("Datos actuales");
                for (int i = 0; i < pila.size(); i++) {
                    System.out.println(pila.get(i));
                }
                System.out.println("Datos impresos exitosamente!");
                break;
            default:
                Menu.main(null);
        }
    }
}
