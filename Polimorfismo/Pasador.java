package Polimorfismo;

/**
 * ING. SISTEMAS COMPUTACIONALES
 *
 * @author JOSIAS LEVID HIDALGO HERNANDEZ
 */
public abstract class Pasador {

    //Constructor
    public Pasador() {
    }

    //METODO A HEREDAR
    public abstract double area();
}
