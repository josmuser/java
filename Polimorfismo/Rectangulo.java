package Polimorfismo;
/**
 * ING. SISTEMAS COMPUTACIONALES 
 * @author JOSIAS LEVID HIDALGO HERNANDEZ
 */
public class Rectangulo extends Pasador {
    //DECLARACION DE VARIABLES

    private double base;
    private double altura;
    
    public Rectangulo(){
        
    }

    public Rectangulo(double base,double altura) {
      this.base=base;
      this.altura=altura;
    }

    public double getAltura() {
        return altura;
    }

    public double getBase() {
        return base;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public void setBase(double base) {
        this.base = base;
    }    
         /**
      * El metodo area() están declarados en la clase base.
      * Estos métodos están redefinidos en las clases derivadas.
      * Se invocan mediante referencias a la clase base Pasador.
      * @return 
      */
    @Override
    public double area() {
        return base*altura;
    }
}
