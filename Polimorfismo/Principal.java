package Polimorfismo;
/**
 * ING. SISTEMAS COMPUTACIONALES 
 * @author JOSIAS LEVID HIDALGO HERNANDEZ
 */
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {

    
    // ArrayList de referencias a objetos de la clase base Poligono
    static ArrayList<Pasador> poligonos = new ArrayList();

    public static void main(String[] args) {
        operacion();
        msgscreen();
    }

    public static void operacion() {
        Scanner datos = new Scanner(System.in);
        int tipo;
        System.out.println("Elija el poligono al cual le quiere sacar su area: ");
        System.out.println("1>Rectangulo \n2>Triangulo");

        try {
            tipo = datos.nextInt();
            if(tipo==1){
                DatosRectangulo();
            }else{
                if(tipo==2){
                    DatosTriangulo();
                }
            }
        } catch (NumberFormatException e) {
            System.out.println("Opcion no valida!! Reintente");
        }
    }

    public static void DatosTriangulo() {
        double base,altura;
        Scanner datoRectan = new Scanner(System.in);
        System.out.println("Ingrese base: ");
         base = datoRectan.nextInt();
         System.out.println("Ingrese altura: ");
         altura= datoRectan.nextInt();
         Triangulo t = new Triangulo(base, altura);
         poligonos.add(t);
    }

    public static void DatosRectangulo() {
        double base1,altura1;
        Scanner datotr = new Scanner(System.in);
        System.out.println("Ingrese base: ");
         base1 = datotr.nextInt();
         System.out.println("Ingrese altura: ");
         altura1= datotr.nextInt();
         Rectangulo r = new Rectangulo(base1, altura1);
         poligonos.add(r);
    }
    public static void msgscreen(){
        //MOSTRANDO Y RECORRIENDO EL ARRAY
        for (Pasador p : poligonos) {
            System.out.printf("Su area es: " + p.area());
        }
    }
}
