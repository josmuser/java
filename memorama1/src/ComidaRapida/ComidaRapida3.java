package ComidaRapida;

import Network.*;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class ComidaRapida3 extends javax.swing.JFrame implements Runnable {

    private reordenar3 reo3 = new reordenar3();
    private boolean dorso = false;
    private boolean cara = false;
    private ImageIcon im1;
    private ImageIcon im2;
    private JButton[] pbtn = new JButton[2];
    private int puntaje = 0;
    //------------------------
    private int seg = 0;
    private int min = 0;
    private int hora = 0;
    private boolean continuar = true;
    //---------------------------------
    private Tiempo3 i;
    public ComidaRapida3() {
        initComponents();
        this.setLocationRelativeTo(null);
        nose();
        txf_cont1.setText(String.valueOf(hora));
        txf_cont2.setText(String.valueOf(min));
        txf_cont3.setText(String.valueOf(seg));

        i = new Tiempo3(this);
        i.start();

    }

    private void nose() {
        int[] numbers = reo3.tomarc();

        jButton1.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[0] + ".png")));
        jButton2.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[1] + ".png")));
        jButton3.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[2] + ".png")));
        jButton4.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[3] + ".png")));
        jButton5.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[4] + ".png")));
        jButton6.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[5] + ".png")));
        jButton7.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[6] + ".png")));
        jButton8.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[7] + ".png")));
        jButton9.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[8] + ".png")));
        jButton10.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[9] + ".png")));
        jButton11.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[10] + ".png")));
        jButton12.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[11] + ".png")));
        jButton13.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[12] + ".png")));
        jButton14.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[13] + ".png")));
        jButton15.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[14] + ".png")));
        jButton16.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[15] + ".png")));
        jButton17.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[16] + ".png")));
        jButton18.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[17] + ".png")));
        jButton19.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[18] + ".png")));
        jButton20.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[19] + ".png")));
        jButton21.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[20] + ".png")));
        jButton22.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[21] + ".png")));
        jButton23.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[22] + ".png")));
        jButton24.setDisabledIcon(new ImageIcon(getClass().getResource("../ComidaRapidaTres/v" + numbers[23] + ".png")));
    }

    private void btnEnabled(JButton btn) {

        if (!cara) {
            btn.setEnabled(false);
            im1 = (ImageIcon) btn.getDisabledIcon();
            pbtn[0] = btn;
            cara = true;
            dorso = false;
        } else {
            btn.setEnabled(false);
            im2 = (ImageIcon) btn.getDisabledIcon();
            pbtn[1] = btn;
            dorso = true;
            puntaje += 20;
            score();
        }
    }

    private void compare() {
        if (cara && dorso) {

            if (im1.getDescription().compareTo(im2.getDescription()) != 0) {
                pbtn[0].setEnabled(true);
                pbtn[1].setEnabled(true);
                if (puntaje > 10) {
                    puntaje -= 10;
                }
            }
            cara = false;
        }
    }

    private void score() {
        if (!jButton1.isEnabled() && !jButton2.isEnabled() && !jButton3.isEnabled() && !jButton4.isEnabled()
                && !jButton5.isEnabled() && !jButton6.isEnabled() && !jButton7.isEnabled() && !jButton8.isEnabled()
                && !jButton9.isEnabled() && !jButton10.isEnabled() && !jButton11.isEnabled() && !jButton12.isEnabled()
                && !jButton13.isEnabled() && !jButton14.isEnabled() && !jButton15.isEnabled() && !jButton16.isEnabled()
                && !jButton17.isEnabled() && !jButton18.isEnabled() && !jButton19.isEnabled() && !jButton20.isEnabled()
                && !jButton21.isEnabled() && !jButton22.isEnabled() && !jButton23.isEnabled() && !jButton24.isEnabled()) {
            JOptionPane.showMessageDialog(this, "Felicidades, usted ha ganado. Su puntaje es: " + puntaje, "Ganador!!",
                    JOptionPane.INFORMATION_MESSAGE);
            reiniciar();
            
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        jButton16 = new javax.swing.JButton();
        ChangeGame = new javax.swing.JButton();
        Reset = new javax.swing.JButton();
        Controls = new javax.swing.JLabel();
        Title = new javax.swing.JLabel();
        jButton17 = new javax.swing.JButton();
        jButton18 = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
        jButton22 = new javax.swing.JButton();
        jButton23 = new javax.swing.JButton();
        jButton24 = new javax.swing.JButton();
        LogoNivel = new javax.swing.JLabel();
        Texto = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txf_cont1 = new javax.swing.JTextField();
        txf_cont2 = new javax.swing.JTextField();
        txf_cont3 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(58, 15, 0));
        jPanel1.setForeground(new java.awt.Color(204, 204, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton1MouseExited(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 260, 100, 130));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton2MouseExited(evt);
            }
        });
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 100, 140));

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton3MouseExited(evt);
            }
        });
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 260, 100, 130));

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton4MouseExited(evt);
            }
        });
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 100, 100, 140));

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton5MouseExited(evt);
            }
        });
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 260, 100, 130));

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton6MouseExited(evt);
            }
        });
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 100, 100, 140));

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton7MouseExited(evt);
            }
        });
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 260, 100, 130));

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton8MouseExited(evt);
            }
        });
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 100, 100, 140));

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton9MouseExited(evt);
            }
        });
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton9, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 260, 100, 130));

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton10MouseExited(evt);
            }
        });
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton10, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 100, 100, 140));

        jButton11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton11MouseExited(evt);
            }
        });
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton11, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 260, 100, 130));

        jButton12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton12MouseExited(evt);
            }
        });
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton12, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 100, 100, 140));

        jButton13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton13MouseExited(evt);
            }
        });
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton13, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 260, 100, 130));

        jButton14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton14MouseExited(evt);
            }
        });
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton14, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 100, 100, 140));

        jButton15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton15MouseExited(evt);
            }
        });
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton15, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 260, 100, 130));

        jButton16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton16MouseExited(evt);
            }
        });
        jButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton16ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton16, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 100, 100, 140));

        ChangeGame.setBackground(new java.awt.Color(153, 255, 0));
        ChangeGame.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        ChangeGame.setIcon(new javax.swing.ImageIcon(getClass().getResource("/BackgroundsUI/Recargar.png"))); // NOI18N
        ChangeGame.setText("Cambiar juego");
        ChangeGame.setBorderPainted(false);
        ChangeGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChangeGameActionPerformed(evt);
            }
        });
        jPanel1.add(ChangeGame, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 590, 170, 40));

        Reset.setBackground(new java.awt.Color(153, 255, 0));
        Reset.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        Reset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/BackgroundsUI/Reinciar.png"))); // NOI18N
        Reset.setText("Reiniciar juego");
        Reset.setBorderPainted(false);
        Reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResetActionPerformed(evt);
            }
        });
        jPanel1.add(Reset, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 590, -1, 40));

        Controls.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        Controls.setForeground(new java.awt.Color(255, 255, 255));
        Controls.setText("Controles");
        jPanel1.add(Controls, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 560, -1, -1));

        Title.setFont(new java.awt.Font("Comic Sans MS", 1, 36)); // NOI18N
        Title.setForeground(new java.awt.Color(255, 255, 255));
        Title.setText("BUSCA LOS PARES DE COMIDA RÁPIDA");
        jPanel1.add(Title, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 30, -1, -1));

        jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton17MouseExited(evt);
            }
        });
        jButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton17ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton17, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 400, 100, 140));

        jButton18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton18MouseExited(evt);
            }
        });
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton18, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 400, 100, 140));

        jButton19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton19MouseExited(evt);
            }
        });
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton19, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 400, 100, 140));

        jButton20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton20MouseExited(evt);
            }
        });
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton20, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 400, 100, 140));

        jButton21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton21MouseExited(evt);
            }
        });
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton21, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 400, 100, 140));

        jButton22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton22MouseExited(evt);
            }
        });
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton22, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 400, 100, 140));

        jButton23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton23MouseExited(evt);
            }
        });
        jButton23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton23ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton23, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 400, 100, 140));

        jButton24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton24MouseExited(evt);
            }
        });
        jButton24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton24ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton24, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 400, 100, 140));

        LogoNivel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/BackgroundsUI/FastFood-logo.png"))); // NOI18N
        jPanel1.add(LogoNivel, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 610, -1, -1));

        Texto.setFont(new java.awt.Font("Ink Free", 1, 36)); // NOI18N
        Texto.setForeground(new java.awt.Color(249, 226, 25));
        Texto.setText("SUERTE!!!!");
        jPanel1.add(Texto, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 560, -1, -1));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Tiempo de juego");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 570, 200, -1));

        txf_cont1.setEditable(false);
        txf_cont1.setBackground(new java.awt.Color(204, 255, 255));
        txf_cont1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        txf_cont1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txf_cont1.setPreferredSize(new java.awt.Dimension(40, 40));
        txf_cont1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txf_cont1ActionPerformed(evt);
            }
        });
        jPanel1.add(txf_cont1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 610, 60, 60));

        txf_cont2.setEditable(false);
        txf_cont2.setBackground(new java.awt.Color(204, 255, 255));
        txf_cont2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        txf_cont2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txf_cont2.setPreferredSize(new java.awt.Dimension(40, 40));
        txf_cont2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txf_cont2ActionPerformed(evt);
            }
        });
        jPanel1.add(txf_cont2, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 610, 60, 60));

        txf_cont3.setEditable(false);
        txf_cont3.setBackground(new java.awt.Color(204, 255, 255));
        txf_cont3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        txf_cont3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txf_cont3.setPreferredSize(new java.awt.Dimension(40, 40));
        txf_cont3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txf_cont3ActionPerformed(evt);
            }
        });
        jPanel1.add(txf_cont3, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 610, 60, 60));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Hora          Min        Seg");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 670, 200, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 943, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        btnEnabled(jButton1);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        btnEnabled(jButton2);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        btnEnabled(jButton3);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        btnEnabled(jButton4);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        btnEnabled(jButton5);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        btnEnabled(jButton6);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        btnEnabled(jButton7);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        btnEnabled(jButton8);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void ResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResetActionPerformed
        reiniciar();
    }//GEN-LAST:event_ResetActionPerformed

    private void jButton1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseExited
        compare();
    }//GEN-LAST:event_jButton1MouseExited

    private void jButton2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseExited
        compare();
    }//GEN-LAST:event_jButton2MouseExited

    private void jButton3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseExited
        compare();
    }//GEN-LAST:event_jButton3MouseExited

    private void jButton4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseExited
        compare();
    }//GEN-LAST:event_jButton4MouseExited

    private void jButton5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton5MouseExited
        compare();
    }//GEN-LAST:event_jButton5MouseExited

    private void jButton6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseExited
        compare();
    }//GEN-LAST:event_jButton6MouseExited

    private void jButton7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton7MouseExited
        compare();
    }//GEN-LAST:event_jButton7MouseExited

    private void jButton8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton8MouseExited
        compare();
    }//GEN-LAST:event_jButton8MouseExited

    private void ChangeGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChangeGameActionPerformed
        new NivelesComida().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_ChangeGameActionPerformed

    private void jButton9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton9MouseExited
        compare();
    }//GEN-LAST:event_jButton9MouseExited

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        btnEnabled(jButton9);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton10MouseExited
        compare();
    }//GEN-LAST:event_jButton10MouseExited

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        btnEnabled(jButton10);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton11MouseExited
        compare();
    }//GEN-LAST:event_jButton11MouseExited

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        btnEnabled(jButton11);
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton12MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton12MouseExited
        compare();
    }//GEN-LAST:event_jButton12MouseExited

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        btnEnabled(jButton12);
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton13MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton13MouseExited
        compare();
    }//GEN-LAST:event_jButton13MouseExited

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        btnEnabled(jButton13);
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton14MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton14MouseExited
        compare();
    }//GEN-LAST:event_jButton14MouseExited

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        btnEnabled(jButton14);
    }//GEN-LAST:event_jButton14ActionPerformed

    private void jButton15MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton15MouseExited
        compare();
    }//GEN-LAST:event_jButton15MouseExited

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        btnEnabled(jButton15);
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jButton16MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton16MouseExited
        compare();
    }//GEN-LAST:event_jButton16MouseExited

    private void jButton16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton16ActionPerformed
        btnEnabled(jButton16);
    }//GEN-LAST:event_jButton16ActionPerformed

    private void jButton17MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton17MouseExited
        compare();
    }//GEN-LAST:event_jButton17MouseExited

    private void jButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton17ActionPerformed
        btnEnabled(jButton17);
    }//GEN-LAST:event_jButton17ActionPerformed

    private void jButton18MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton18MouseExited
        compare();
    }//GEN-LAST:event_jButton18MouseExited

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        btnEnabled(jButton18);
    }//GEN-LAST:event_jButton18ActionPerformed

    private void jButton19MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton19MouseExited
        compare();
    }//GEN-LAST:event_jButton19MouseExited

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
        btnEnabled(jButton19);
    }//GEN-LAST:event_jButton19ActionPerformed

    private void jButton20MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton20MouseExited
        compare();
    }//GEN-LAST:event_jButton20MouseExited

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
        btnEnabled(jButton20);
    }//GEN-LAST:event_jButton20ActionPerformed

    private void jButton21MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton21MouseExited
        compare();
    }//GEN-LAST:event_jButton21MouseExited

    private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton21ActionPerformed
        btnEnabled(jButton21);
    }//GEN-LAST:event_jButton21ActionPerformed

    private void jButton22MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton22MouseExited
        compare();
    }//GEN-LAST:event_jButton22MouseExited

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        btnEnabled(jButton22);
    }//GEN-LAST:event_jButton22ActionPerformed

    private void jButton23MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton23MouseExited
        compare();
    }//GEN-LAST:event_jButton23MouseExited

    private void jButton23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton23ActionPerformed
        btnEnabled(jButton23);
    }//GEN-LAST:event_jButton23ActionPerformed

    private void jButton24MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton24MouseExited
        compare();
    }//GEN-LAST:event_jButton24MouseExited

    private void jButton24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton24ActionPerformed
        btnEnabled(jButton24);
    }//GEN-LAST:event_jButton24ActionPerformed

    private void txf_cont1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txf_cont1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txf_cont1ActionPerformed

    private void txf_cont2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txf_cont2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txf_cont2ActionPerformed

    private void txf_cont3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txf_cont3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txf_cont3ActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            new ComidaRapida3().setVisible(true);
        });
    }

    private void reiniciar() {

        jButton1.setEnabled(true);
        jButton2.setEnabled(true);
        jButton3.setEnabled(true);
        jButton4.setEnabled(true);
        jButton5.setEnabled(true);
        jButton6.setEnabled(true);
        jButton7.setEnabled(true);
        jButton8.setEnabled(true);
        jButton9.setEnabled(true);
        jButton10.setEnabled(true);
        jButton11.setEnabled(true);
        jButton12.setEnabled(true);
        jButton13.setEnabled(true);
        jButton14.setEnabled(true);
        jButton15.setEnabled(true);
        jButton16.setEnabled(true);
        jButton17.setEnabled(true);
        jButton18.setEnabled(true);
        jButton19.setEnabled(true);
        jButton20.setEnabled(true);
        jButton21.setEnabled(true);
        jButton22.setEnabled(true);
        jButton23.setEnabled(true);
        jButton24.setEnabled(true);
        dorso = false;
        cara = false;
        puntaje = 0;
        nose();
        resetSeg();
        resetMin();
        resetHora();
        i.start();
        parar();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ChangeGame;
    private javax.swing.JLabel Controls;
    private javax.swing.JLabel LogoNivel;
    private javax.swing.JButton Reset;
    private javax.swing.JLabel Texto;
    private javax.swing.JLabel Title;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txf_cont1;
    private javax.swing.JTextField txf_cont2;
    private javax.swing.JTextField txf_cont3;
    // End of variables declaration//GEN-END:variables
public synchronized int aumentSeg() {
        seg++;
        txf_cont3.setText(String.valueOf(seg));
        return seg;
    }

    public synchronized int aumentMin() {
        min++;
        txf_cont2.setText(String.valueOf(min));
        return min;
    }

    public synchronized int aumentHora() {
        hora++;
        txf_cont1.setText(String.valueOf(hora));
        return hora;
    }

    public void resetSeg() {
        txf_cont3.setText(String.valueOf("0"));
        seg = 0;
    }

    public void resetMin() {
        txf_cont2.setText(String.valueOf("0"));
        min = 0;
    }

    public void resetHora() {
        txf_cont1.setText(String.valueOf("0"));
        hora = 0;
    }

    public synchronized int getMin() {
        return min;
    }

    public synchronized int getSeg() {
        return seg;
    }

    public synchronized int getHora() {
        return hora;
    }

    public synchronized void seguir() {
        continuar = true;
    }

    public synchronized void parar() {
        continuar = false;
    }

    public synchronized boolean isContinuar() {
        return continuar;
    }

@Override
    public void run() {
       
    }
}
