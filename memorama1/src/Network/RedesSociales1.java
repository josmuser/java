package Network;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;



public class RedesSociales1 extends javax.swing.JFrame implements Runnable{
private final reordenar reo = new reordenar();
private boolean dorso = false;
private boolean cara = false;
private ImageIcon im1;
private ImageIcon im2;
private final JButton[] pbtn = new JButton[2];
private int puntaje = 0;
//------------------------
    private int seg = 0;
    private int min = 0;
    private int hora = 0;
    private boolean continuar = true;
    //---------------------------------
    private Tiempo1 i;

    public RedesSociales1() {
        initComponents();
        this.setLocationRelativeTo(null);
        nose();
        txf_cont1.setText(String.valueOf(hora));
        txf_cont2.setText(String.valueOf(min));
        txf_cont3.setText(String.valueOf(seg));

        i = new Tiempo1(this);
        i.start();

    }
 
 private void nose() {
        int[] numbers = reo.tomarc();
        
        jButton1.setDisabledIcon(new ImageIcon(getClass().getResource("/RedesUno/d"+numbers[0]+".png")));
        jButton2.setDisabledIcon(new ImageIcon(getClass().getResource("/RedesUno/d"+numbers[1]+".png")));
        jButton3.setDisabledIcon(new ImageIcon(getClass().getResource("/RedesUno/d"+numbers[2]+".png")));
        jButton4.setDisabledIcon(new ImageIcon(getClass().getResource("/RedesUno/d"+numbers[3]+".png")));
        jButton5.setDisabledIcon(new ImageIcon(getClass().getResource("/RedesUno/d"+numbers[4]+".png")));
        jButton6.setDisabledIcon(new ImageIcon(getClass().getResource("/RedesUno/d"+numbers[5]+".png")));
        jButton7.setDisabledIcon(new ImageIcon(getClass().getResource("/RedesUno/d"+numbers[6]+".png")));
        jButton8.setDisabledIcon(new ImageIcon(getClass().getResource("/RedesUno/d"+numbers[7]+".png")));
        

    }  

     private void btnEnabled(JButton btn) {
        
        if(!cara) {
            btn.setEnabled(false);
            im1 = (ImageIcon) btn.getDisabledIcon();
            pbtn[0] = btn;
            cara = true;
            dorso = false;
        }
        else {
            btn.setEnabled(false);
            im2 = (ImageIcon) btn.getDisabledIcon();
            pbtn[1] = btn;
            dorso = true;
            puntaje += 20;
            score();
        }
    }
         private void compare() {
        if(cara && dorso) {
            
            if(im1.getDescription().compareTo(im2.getDescription()) != 0) {
                pbtn[0].setEnabled(true);
                pbtn[1].setEnabled(true);
                if(puntaje > 10 ) puntaje -= 10;
            }
            cara = false;
        }
    }
         private void score() {
            if(!jButton1.isEnabled() && !jButton2.isEnabled() && !jButton3.isEnabled() && !jButton4.isEnabled() && 
               !jButton5.isEnabled() && !jButton6.isEnabled() && !jButton7.isEnabled() && !jButton8.isEnabled()) {
            JOptionPane.showMessageDialog(this, "Felicidades, usted ha ganado. Su puntaje es: "+puntaje, "Ganador!!", 
            JOptionPane.INFORMATION_MESSAGE);
            reiniciar();
            
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        ChangeGame = new javax.swing.JButton();
        LogoNivel = new javax.swing.JLabel();
        Texto = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txf_cont1 = new javax.swing.JTextField();
        txf_cont2 = new javax.swing.JTextField();
        txf_cont3 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(4, 166, 212));
        jPanel1.setForeground(new java.awt.Color(204, 204, 255));
        jPanel1.setName("Nivel 1 Redes Sociales"); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton1MouseExited(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 290, -1, -1));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton2MouseExited(evt);
            }
        });
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, -1, -1));

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton3MouseExited(evt);
            }
        });
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 290, -1, -1));

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton4MouseExited(evt);
            }
        });
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 100, -1, -1));

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton5MouseExited(evt);
            }
        });
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 290, -1, -1));

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton6MouseExited(evt);
            }
        });
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 100, -1, -1));

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton7MouseExited(evt);
            }
        });
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 290, -1, -1));

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/F0.png"))); // NOI18N
        jButton8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton8MouseExited(evt);
            }
        });
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 100, -1, -1));

        jButton9.setBackground(new java.awt.Color(133, 255, 236));
        jButton9.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/BackgroundsUI/Reinciar.png"))); // NOI18N
        jButton9.setText("Reiniciar juego");
        jButton9.setBorderPainted(false);
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton9, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 510, -1, 40));

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("BUSCA LOS PARES DE REDES SOCIALES");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 40, -1, -1));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel2.setText("Controles");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 480, -1, -1));

        ChangeGame.setBackground(new java.awt.Color(133, 255, 236));
        ChangeGame.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        ChangeGame.setIcon(new javax.swing.ImageIcon(getClass().getResource("/BackgroundsUI/Recargar.png"))); // NOI18N
        ChangeGame.setText("Regresar");
        ChangeGame.setBorderPainted(false);
        ChangeGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChangeGameActionPerformed(evt);
            }
        });
        jPanel1.add(ChangeGame, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 510, 170, 40));

        LogoNivel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/BackgroundsUI/movile-level.png"))); // NOI18N
        jPanel1.add(LogoNivel, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 460, 90, 80));

        Texto.setFont(new java.awt.Font("Ink Free", 1, 36)); // NOI18N
        Texto.setForeground(new java.awt.Color(249, 226, 25));
        Texto.setText("SUERTE!!!!");
        jPanel1.add(Texto, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 530, -1, -1));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Tiempo de juego");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 460, -1, -1));

        txf_cont1.setEditable(false);
        txf_cont1.setBackground(new java.awt.Color(204, 255, 255));
        txf_cont1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        txf_cont1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txf_cont1.setPreferredSize(new java.awt.Dimension(40, 40));
        txf_cont1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txf_cont1ActionPerformed(evt);
            }
        });
        jPanel1.add(txf_cont1, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 500, 60, 60));

        txf_cont2.setEditable(false);
        txf_cont2.setBackground(new java.awt.Color(204, 255, 255));
        txf_cont2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        txf_cont2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txf_cont2.setPreferredSize(new java.awt.Dimension(40, 40));
        txf_cont2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txf_cont2ActionPerformed(evt);
            }
        });
        jPanel1.add(txf_cont2, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 500, 60, 60));

        txf_cont3.setEditable(false);
        txf_cont3.setBackground(new java.awt.Color(204, 255, 255));
        txf_cont3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        txf_cont3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txf_cont3.setPreferredSize(new java.awt.Dimension(40, 40));
        txf_cont3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txf_cont3ActionPerformed(evt);
            }
        });
        jPanel1.add(txf_cont3, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 500, 60, 60));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Hora          Min        Seg");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 560, 200, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 919, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        btnEnabled(jButton1);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        btnEnabled(jButton2);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        btnEnabled(jButton3);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        btnEnabled(jButton4);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        btnEnabled(jButton5);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        btnEnabled(jButton6);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        btnEnabled(jButton7);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        btnEnabled(jButton8);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
       reiniciar();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseExited
        compare();
    }//GEN-LAST:event_jButton1MouseExited

    private void jButton2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseExited
        compare();
    }//GEN-LAST:event_jButton2MouseExited

    private void jButton3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseExited
        compare();
    }//GEN-LAST:event_jButton3MouseExited

    private void jButton4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseExited
        compare();
    }//GEN-LAST:event_jButton4MouseExited

    private void jButton5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton5MouseExited
        compare();
    }//GEN-LAST:event_jButton5MouseExited

    private void jButton6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseExited
        compare();
    }//GEN-LAST:event_jButton6MouseExited

    private void jButton7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton7MouseExited
        compare();
    }//GEN-LAST:event_jButton7MouseExited

    private void jButton8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton8MouseExited
        compare();
    }//GEN-LAST:event_jButton8MouseExited

    private void ChangeGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChangeGameActionPerformed
     NivelesNetworks select = new NivelesNetworks();
        select.setVisible(true);
        this.setVisible(false);
        
    }//GEN-LAST:event_ChangeGameActionPerformed

    private void txf_cont1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txf_cont1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txf_cont1ActionPerformed

    private void txf_cont2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txf_cont2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txf_cont2ActionPerformed

    private void txf_cont3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txf_cont3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txf_cont3ActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            new RedesSociales1().setVisible(true);
        });
    }
 
        private void reiniciar() {
        
        jButton1.setEnabled(true);
        jButton2.setEnabled(true);
        jButton3.setEnabled(true);
        jButton4.setEnabled(true);
        jButton5.setEnabled(true);
        jButton6.setEnabled(true);
        jButton7.setEnabled(true);
        jButton8.setEnabled(true);
        resetSeg();
        resetMin();
        resetHora();
        i.start();
        parar();
        dorso = false;
        cara = false;
        puntaje = 0;
        nose();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ChangeGame;
    private javax.swing.JLabel LogoNivel;
    private javax.swing.JLabel Texto;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txf_cont1;
    private javax.swing.JTextField txf_cont2;
    private javax.swing.JTextField txf_cont3;
    // End of variables declaration//GEN-END:variables
public synchronized int aumentSeg() {
        seg++;
        txf_cont3.setText(String.valueOf(seg));
        return seg;
    }

    public synchronized int aumentMin() {
        min++;
        txf_cont2.setText(String.valueOf(min));
        return min;
    }

    public synchronized int aumentHora() {
        hora++;
        txf_cont1.setText(String.valueOf(hora));
        return hora;
    }

    public void resetSeg() {
        txf_cont3.setText(String.valueOf("0"));
        seg = 0;
    }

    public void resetMin() {
        txf_cont2.setText(String.valueOf("0"));
        min = 0;
    }

    public void resetHora() {
        txf_cont1.setText(String.valueOf("0"));
        hora = 0;
    }

    public synchronized int getMin() {
        return min;
    }

    public synchronized int getSeg() {
        return seg;
    }

    public synchronized int getHora() {
        return hora;
    }

    public synchronized void seguir() {
        continuar = true;
    }

    public synchronized void parar() {
        continuar = false;
    }

    public synchronized boolean isContinuar() {
        return continuar;
    }

    @Override
    public void run() {

    }
}
