package Libro2;
/**
 *
 * @author HWCode
 */
public class OperadoresAritmeticosBinarios {

    public static void main(String[] args) {
        int n1 = 15;
        int n2 = 5;
        /**
         * En esta parte se aplica un operador de aritmetico binario suma(+)
         * para poder llegar a sumar y obtener el resultado estimado.
         */
        //Operador (+) suma
        int n3 = n1 + n2;
        //Operador (-) resta
        int n4 = n3 - n2;
        //Operador (*) multiplicacion 
        int n5 = n4 * n1;
        //Operador (/) division
        int n6 = n5 / n2;
        //Operador (%) modulo
        int n7=n5%n2;
        //El esta parte el operador de suma, solo concatenara no sumara
        System.out.println("La suma es: " + n3);
        System.out.println("La resta es: " + n4);
        System.out.println("La multiplicacion es: " + n5);
        System.out.println("La division es: " + n6);
        System.out.println("El residuo es: "+ n7);
    }
}
