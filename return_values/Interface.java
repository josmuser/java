package return_values;

import java.util.Scanner;

public class Interface {
	@SuppressWarnings({ "resource", "static-access" })
	public static void main(String[] args) {
		/*
		 * En este programa se plantea sumar varios numeros los cuales son enviados a
		 * otro arraylist para ser procesados y sumados y estos ser retornados con un
		 * valor al final Se hace un llamada de un arraylist desde otra clase. Esto
		 * aplicando lo del libro <introduccion al lenguaje de programacion java por la
		 * UNAM
		 * Pag. 52 
		 * Ejercicio de retorno de datos
		 * 
		 */
		source obj = new source();
		Scanner teclado = new Scanner(System.in);
		Scanner ncantidad = new Scanner(System.in);
		Scanner decisionx = new Scanner(System.in);
		
		int max;
		int varc;

		
		try {
			System.out.println("\nPrograma de sumas multiples");
			System.out.println("Que desea hacer?\n1> sumar\n2>salir");
			int decisionsx = decisionx.nextInt();
			if (decisionsx==1) {
				try {
					System.out.println("Cuantos numeros desea sumar:");
					max = teclado.nextInt();
					for (int i = 0; i < max; i++) {
						System.out.println("Nuevo numero:");
						varc = ncantidad.nextInt();
						obj.insertar(varc);
					}	
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("Solo se aceptan numeros porfavor!\n");
					System.out.println("Suma invalidad! Reintente porfavor!\n");
				}
			}else {
				if (decisionsx==2) {
					decisionsx=0;
					System.out.println("Programa terminado");
					System.exit(decisionsx);
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Opcion invalida!\n");
			Interface.main(null);
		}

		
		int imprimirdatos = obj.obtener();

		System.out.println("Suma da: " + imprimirdatos +"\n");
		Interface.main(null);
		System.out.flush();
		teclado.close();
		ncantidad.close();
		decisionx.close();
	}

}