/**
 * INSTITUTO TECNOLOGICO SUPERIOR DE CHAMPOTÓN
 * ING. SISTEMAS COMPUTACIONALES
 * MATERIA: PROGRAMACION ORIENTADA A OBJETOS
 * PARA BITBUCKET
 * EJEMPLO EN JAVA
 * SUBIDO POR JOSMUSER
 */
package factorial_numero;

import java.util.Scanner;

public class Usuario {

    public static void main(String[] args) {
        //OBJETO FACTORIAL
        Factorial usuario = new Factorial();
        Scanner opcion = new Scanner(System.in);
        String definido_opcion;//OPCION PARA SALIR O ENTRAR AL PROGRAMA
        System.out.println("***Este programa saca varias factoriales***");
        System.out.println("¿Quiere sacar factoriales? Escriba" + " si " + " o " + " no ");
        definido_opcion = opcion.nextLine();
        switch (definido_opcion) {
            case "si":
                usuario.Operacion();
                break;
            case "no":
                System.out.println("Programa finalizado...");
                String[] opciones = {"INSTITUTO TECNOLOGICO SUPERIOR DE CHAMPOTÓN", "ING. SISTEMAS COMPUTACIONALES", "MATERIA: PROGRAMACION ORIENTADA A OBJETOS", "DOCENTE: KELVIN DEL JESÚS DELGADO CHAN", "ALUMNO: JOSIAS LEVID HIDALGO HERNANDEZ", "MATRICULA: 201080104"};
                for (int i = 0; i < opciones.length; i++) {
                    System.out.println(opciones[i]);
                }
                System.exit(0);
                break;
            default:
                Usuario.main(null);
        }
    }
}
