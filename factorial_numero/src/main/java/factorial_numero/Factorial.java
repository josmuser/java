/**
 * INSTITUTO TECNOLOGICO SUPERIOR DE CHAMPOTÓN
 * ING. SISTEMAS COMPUTACIONALES
 * MATERIA: PROGRAMACION ORIENTADA A OBJETOS
 * PARA BITBUCKET
 * EJEMPLO EN JAVA
 * SUBIDO POR JOSMUSER
 */
package factorial_numero;

import java.util.ArrayList;
import java.util.Scanner;

public class Factorial {
        
   
        //SE DECLARA METODOS
        int tamanio;
        int productoriafinal;
        //CONSTRUCTOR
        public Factorial(){
            productoriafinal=productoriafinal;
        }
        //METODOS
        public void Operacion(){
        ArrayList<Integer> lista = new ArrayList<>();
        //UN NUEVO SCANNER PARA DATOS
        Scanner dato_longitud = new Scanner(System.in);
        Scanner dato_ingresados = new Scanner(System.in);
        Scanner salir = new Scanner(System.in);
        System.out.println("¿ A cuentos numeros le quiere sacar su factorial ?");
        tamanio = dato_longitud.nextInt();
        int res = 0;
        for (int  i = 1; i <= tamanio; i++) {
            System.out.println("Digite su numero [" + i+"]");
            int num = dato_ingresados.nextInt();
            lista.add(num);
        }
        for (Integer e : lista) {
            System.out.println("El factorial de su numero es :"+ sacando_fact(e));
        }  
        String definido_opcion;
        System.out.println("**************************");
        System.out.println("¿Quiere salir del programa? Escriba "+"si"+" o "+"no");
        definido_opcion=salir.nextLine();
        switch(definido_opcion){
        case "si":
            System.out.println("Programa finalizado...");
            System.exit(0);
            break;
        case "no":
            Usuario.main(null);
            break;
            default:
               System.out.println("Programa cerrado inesperadamente...");   
               System.exit(0);
        }
        }
    public static int sacando_fact(int numero) {
        int productoria = 1;
        for (int i = 1; i <= numero; i++) {
            productoria = productoria * i;
        }
       
        return productoria;
    }
}
